document.addEventListener('DOMContentLoaded', function () {
  const getHereRewardingButton = document.getElementById("get-here-rewarding-button");

  getHereRewardingButton.addEventListener("click", function (e) {
    e.preventDefault(); // Prevent the default form submission

    
    // Get the email input value
    const userEmail = document.getElementById("user-email").value;
    
    // Make an AJAX request
    const xhr = new XMLHttpRequest();
    xhr.open("POST", popup_script_vars.ajax_url, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    
    // Generate a nonce value using the localized nonce provided by WordPress
    const nonce = popup_script_vars.nonce;
    
    // Prepare the data to be sent
    const data = "action=gftp_rewarding_custom_api&custom_popup_nonce_field=" + nonce + "&user_email=" + userEmail;

    xhr.onreadystatechange = function () {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        console.log("Ready state: " + xhr.readyState);
        console.log("Status: " + xhr.status);
        console.log("Response: " + xhr.responseText);

        if (xhr.status === 200) {
          // Log the response for debugging
          console.log("Response from server: " + xhr.responseText);
        }
      }
    };
    
    xhr.send(data);
  });
});

