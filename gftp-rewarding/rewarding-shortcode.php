<?php
function gftp_rewarding_shortcode($atts, $content = null)
{
  // Load the content of the popup HTML file
  $rewarding_content = file_get_contents(plugin_dir_path(__FILE__) . 'rewarding-content.html');

  // Replace {{content}} placeholder with the provided content
  $rewarding_content = str_replace('{{content}}', $content, $rewarding_content);

  // Create the final popup output
  $output = '<div id="rewarding">' . $rewarding_content . '</div>';
  return $output;
}
add_shortcode('gftp_rewarding_shortcode', 'gftp_rewarding_shortcode');