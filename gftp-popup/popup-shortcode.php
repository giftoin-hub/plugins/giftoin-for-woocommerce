<?php
function gftp_popup_shortcode($atts, $content = null)
{
  // Load the content of the popup HTML file
  $popup_content = file_get_contents(plugin_dir_path(__FILE__) . 'popup-content.html');

  // Get the custom popup image URL, title, body
  $gftp_custom_popup_image = get_option('gftp_custom_popup_image', 'https://firebasestorage.googleapis.com/v0/b/gt-org-prod.appspot.com/o/alon%2FGiftoin%20for%20NFT%20-%201.jpg?alt=media&token=8b6ec020-6428-4a17-b03f-df8f7e1b91d6');
  $gftp_custom_popup_title = get_option('gftp_custom_popup_title', 'Get Exclusive Rewards!');
  $gftp_custom_popup_body = get_option('gftp_custom_popup_body', 'Claim your special gift now by clicking the "Get Here!" button below.');

  // Get the current logged-in user's email and username
  $current_user = wp_get_current_user();
  $current_user_email = $current_user->user_email;
  $current_user_name = $current_user->display_name;

  $popup_content = str_replace('{user-name}', $current_user_name, $popup_content);

  $popup_content = str_replace('{user-email}', $current_user_email, $popup_content);

  // Replace {{image_src}} placeholder with the custom popup image URL
  $popup_content = str_replace('{{image_src}}', $gftp_custom_popup_image, $popup_content);

  $popup_content = str_replace('{{popup_title}}', $gftp_custom_popup_title, $popup_content);

  $popup_content = str_replace('{{popup_body}}', $gftp_custom_popup_body, $popup_content);

  // Replace {{content}} placeholder with the provided content
  $popup_content = str_replace('{{content}}', $content, $popup_content);

  // Create the final popup output
  $output = '<div id="popup" class="modal">' . $popup_content . '</div>';
  return $output;
}
add_shortcode('gftp_popup_shortcode', 'gftp_popup_shortcode');