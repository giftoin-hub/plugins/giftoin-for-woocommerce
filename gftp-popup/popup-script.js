document.addEventListener('DOMContentLoaded', function () {
  var modal = document.getElementById('popup');
  var modalContent = document.querySelector('.modal-content');
  var closeButton = document.querySelector('.modal-close');

  // Open the modal with a delay and fade-in animation
  function openModalWithDelay(delay) {
    setTimeout(function () {
      modal.style.display = 'block';
      modal.style.opacity = '0'; // Set initial opacity to 0
      gftpFadeInModal(1, 300); // Fade in the modal to full opacity over 300ms
    }, delay);
  }

  // Fade in the modal gradually
  function gftpFadeInModal(targetOpacity, duration) {
    var start = null;
    var initialOpacity = parseFloat(modal.style.opacity || 0);
    function animate(timestamp) {
      if (!start) start = timestamp;
      var progress = timestamp - start;
      modal.style.opacity = (initialOpacity + (progress / duration) * (targetOpacity - initialOpacity)).toString();
      if (progress < duration) {
        requestAnimationFrame(animate);
      }
    }
    requestAnimationFrame(animate);
  }

  // Close the modal
  function gftpCloseModal() {
    modal.style.display = 'none';
  }

  // Close the modal when clicking outside of it
  window.addEventListener('click', function (event) {
    if (event.target === modal) {
      gftpCloseModal();
    }
  });

  // Close the modal when clicking the close button
  closeButton?.addEventListener('click', function () {
    gftpCloseModal();
  });

  // Open the modal after a delay (e.g., 3000 milliseconds or 3 seconds)
  openModalWithDelay(3000);

  // TODO: NEW CODE
  const getHereButton = document.getElementById("get-here-button");

  getHereButton.addEventListener("click", function () {
    // Make an AJAX request
    const xhr = new XMLHttpRequest();
    xhr.open("POST", popup_script_vars.ajax_url, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

    // Generate a nonce value using the localized nonce provided by WordPress
    const nonce = popup_script_vars.nonce;

    // Prepare the data to be sent
    const data = "action=trigger_custom_api&custom_popup_nonce_field=" + nonce;

    xhr.onreadystatechange = function () {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          // Log the response for debugging
          console.log("Response from server: " + xhr.responseText);
        }
      }
    };

    xhr.send(data);
  });

  // Handle form submission
  const emailForm = document.getElementById("email-form");

  emailForm.addEventListener("submit", function (event) {
    event.preventDefault(); // Prevent the default form submission
  
    const userEmail = document.getElementById("user-email").value.trim(); // Get the user's email from the input field

    // Invalid email
    if (!gftpIsValidEmail(userEmail)) {
      return;
    };
  
    // Make an AJAX request to the server-side PHP function
    jQuery.ajax({
      type: "POST",
      url: popup_script_vars.ajax_url,
      data: {
        action: "popup_submit", // Name of the server-side PHP function
        user_email: userEmail, // Pass user's email to the server
        custom_popup_nonce_field: popup_script_vars.nonce,
      },
      success: function (response) {
        console.log("Server Response: " + response);
        // Handle the response from the server as needed
      },
    });
  });

  function gftpIsValidEmail(email) {
    // Regular expression for basic email format validation
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }
});

