<div class="section getting-started">
  <div class="nsl-admin-sub-content">
    <div class="nsl-admin-getting-started">
      <h2 class="title">Getting Started</h2>

      <p>The Giftoin WordPress plugin allows you to integrate the Giftoin API with your WooCommerce store. With this plugin, you can trigger API requests based on specific actions and manage triggers for your Giftoin campaign.</p>
      <p><u>Documentation can be found on <a href="https://docs.giftoin.com/" target="_blank">Documentation</a></u>.</p>

      <p></p>
      <p><strong><u>Warning</u></strong>: Providers change the App setup process quite often, which means some steps below might not be accurate. If you see significant difference in the written instructions and what you see at the provider, feel free to <a href="https://giftoin.com/Giftoin_Discovery_Call" target="_blank">Report It</a>, so we can check and update the instructions.<br><strong>Last updated:</strong> September 27, 2023.</p>
      <p></p>

      <h2 class="title">Add Giftoin Campaigns</h2>

      <ol>
        <li>Navigate to <a href="https://reseller.giftoin.org/" target="_blank">https://reseller.giftoin.org/</a></li>
        <li>Log in with your credentials if you are not logged in.</li>
        <li>Go to "<b>Settings</b>" and inside click on "<b>Account</b>".</li>
        <li>Copy the "<b>API Key</b>".</li>
        <li><b>Optional</b>: Click on the "<b>Campaigns</b>" button and in the page choose the "<b>New Campaign</b>"</li>
        <li><b>Optional</b>: Create a new campaign using the wizard ("Fixed" campaign for custom triggers, "Dynamic" for cashback) "<b>Register Now</b>" button, fill the form then finally verify your account.</li>
        <li>Copy the desired campaign from the "<b>Campaigns</b>" tab.</li>
        <li>Go to "<b>Settings</b>" tab in the giftoin plugin.</li>
        <li>Enter your "<b>Reseller API Key</b>" in "<b>General</b>" section.</li>
        <li><b>Optional</b>: Add cashback campaign ("<b>Coin Id</b>" can be found in the "<b>Reseller Dashboard</b>" in the example API call of the campaign you selected.).</li>
        <li><b>Optional</b>: Add "<b>Opt-In (Popup)</b>" and paste it in the desired page.</li>
        <li><b>Optional</b>: Add "<b>Custom Trigger Settings</b>", Click on "Add Trigger" choose trigger from the list and add campaign to go with it.</li>
        <li>Click on "<b>Save Settings</b>" to save new changes.</li>
      </ol>

      <p><b>WARNING:</b> <u>Don't share your <b>Reseller API Key</b> with anyone!</u>.<br>
        If you did or lost yours please contact us at <a href="https://giftoin.com/Giftoin_Discovery_Call" target="_blank">Contact Us</a>.</p>

      <br>
    </div>
  </div>
</div>