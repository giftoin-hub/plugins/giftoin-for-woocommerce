<?php
function gftp_plugin_page()
{
    if (current_user_can('manage_options') === false) {
        return; // Only administrators can see the page content.
    }

    //* General Settings
    //* --------------------------
    // Handle API key update.
    if (isset($_POST['gftp_api_key'])) {
        $gftp_api_key = sanitize_text_field($_POST['gftp_api_key']);
        update_option('gftp_api_key', $gftp_api_key);
    }

    //* Cashback Settings
    //* --------------------------
    //* - Crypto Currency Campaign
    // Handle campaign ID update.
    if (isset($_POST['gftp_campaign_id'])) {
        $gftp_campaign_id = sanitize_text_field($_POST['gftp_campaign_id']);
        update_option('custom_popup_campaign_id', $gftp_campaign_id);
    }

    // Handle cashback campaign ID update.
    if (isset($_POST['gftp_products_tag'])) {
        $gftp_products_tag = sanitize_text_field($_POST['gftp_products_tag']);
        update_option('gftp_products_tag', $gftp_products_tag);
    }

    // Handle cashback percentage update.
    if (isset($_POST['gftp_cashback_percentage'])) {
        $gftp_cashback_percentage = floatval($_POST['gftp_cashback_percentage']);
        update_option('gftp_cashback_percentage', $gftp_cashback_percentage);
    }

    // Handle cashback campaign ID update.
    if (isset($_POST['gftp_cashback_campaign_id'])) {
        $gftp_cashback_campaign_id = sanitize_text_field($_POST['gftp_cashback_campaign_id']);
        update_option('gftp_cashback_campaign_id', $gftp_cashback_campaign_id);
    }

    // Handle cashback campaign ID update.
    if (isset($_POST['gftp_cashback_coin_id'])) {
        $gftp_cashback_coin_id = sanitize_text_field($_POST['gftp_cashback_coin_id']);
        update_option('gftp_cashback_coin_id', $gftp_cashback_coin_id);
    }

    //* - Custom Token Campaign (CT)
    //* --------------------------
    // Handle cashback campaign ID update.
    if (isset($_POST['gftp_ct_cashback_campaign_id'])) {
        $gftp_ct_cashback_campaign_id = sanitize_text_field($_POST['gftp_ct_cashback_campaign_id']);
        update_option('gftp_ct_cashback_campaign_id', $gftp_ct_cashback_campaign_id);
    }

    // Handle cashback campaign ID update.
    if (isset($_POST['gftp_ct_products_tag'])) {
        $gftp_ct_products_tag = sanitize_text_field($_POST['gftp_ct_products_tag']);
        update_option('gftp_ct_products_tag', $gftp_ct_products_tag);
    }

    // Handle cashback percentage update.
    if (isset($_POST['gftp_ct_cashback_percentage'])) {
        $gftp_ct_cashback_percentage = floatval($_POST['gftp_ct_cashback_percentage']);
        update_option('gftp_ct_cashback_percentage', $gftp_ct_cashback_percentage);
    }


    // Handle cashback campaign ID update.
    if (isset($_POST['gftp_ct_cashback_coin_id'])) {
        $gftp_ct_cashback_coin_id = sanitize_text_field($_POST['gftp_ct_cashback_coin_id']);
        update_option('gftp_ct_cashback_coin_id', $gftp_ct_cashback_coin_id);
    }

    //* Opt-in Settings
    //* --------------------------
    // Handle campaign id update.
    if (isset($_POST['gftp_rewarding_campaign_id'])) {
        $gftp_rewarding_campaign_id = sanitize_text_field($_POST['gftp_rewarding_campaign_id']);
        update_option('gftp_custom_rewarding_campaign_id', $gftp_rewarding_campaign_id);
    }

    echo '<div class="updated"><p>Settings have been updated.</p></div>';

    $gftp_api_key = get_option('gftp_api_key', '');

    $gftp_cashback_campaign_id = get_option('gftp_cashback_campaign_id', '');
    $gftp_cashback_percentage = get_option('gftp_cashback_percentage', 5.0); // Default cashback percentage.
    $gftp_cashback_coin_id = get_option('gftp_cashback_coin_id'); // Coin id.
    $gftp_products_tag = get_option('gftp_products_tag', ''); // Products tag.

    $gftp_ct_cashback_campaign_id = get_option('gftp_ct_cashback_campaign_id', '');
    $gftp_ct_cashback_percentage = get_option('gftp_ct_cashback_percentage', 5.0); // Default cashback percentage.
    $gftp_ct_cashback_coin_id = get_option('gftp_ct_cashback_coin_id'); // Coin id.
    $gftp_ct_products_tag = get_option('gftp_ct_products_tag', ''); // Products tag.

    $gftp_custom_rewarding_campaign_id = get_option('gftp_custom_rewarding_campaign_id', '');

    // Include the separated HTML code.
    include('admin-page.php');

?>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const revealApiKeyCheckbox = document.getElementById("reveal_api_key");
            const apiKeyInput = document.getElementById("gftp_api_key");

            // Declare triggerForm in an outer scope.
            let triggerForm;

            // Define a flag to track if the trigger form is already displayed.
            let triggerFormDisplayed = false;

            // Call the function to display current triggers.
            gftpDisplayCurrentTriggers('<?php echo esc_js(wp_create_nonce('custom_popup_nonce')); ?>');

            // Define trigger actions.
            const triggerActions = {
                'purchase': 'woocommerce_new_order',
                'add_to_cart': 'woocommerce_add_to_cart',
            };

            // Create an array to store triggers.
            // const triggers = get_option('custom_popup_triggers', []);.
            const triggers = [];

            revealApiKeyCheckbox.addEventListener("change", function() {
                apiKeyInput.type = this.checked ? "text" : "password";
            });

            const addTriggerButton = document.getElementById("add_trigger_btn");
            const triggerSettingsSection = document.querySelector(".trigger-settings");

            addTriggerButton.addEventListener("click", function(event) {
                event.preventDefault(); // Prevent the default behavior.

                if (triggerFormDisplayed === false) {
                    // Set padding for the container.
                    triggerSettingsSection.style.paddingBottom = "10px"; // Add the padding here.

                    // Create and append the trigger form.
                    triggerForm = gftpCreateTriggerForm();
                    triggerSettingsSection.appendChild(triggerForm);

                    // Set the flag to indicate that the form is displayed.
                    triggerFormDisplayed = true;

                    // Disable the "Add Trigger" button.
                    addTriggerButton.disabled = true;

                    gftpCheckTriggerLimit(); // Check trigger limit when displaying the form.
                }

                return false;
            });

            // AJAX function to save a new trigger.
            function gftpSaveTriggerToWordPress(selectedTrigger, campaignId) {
                // Create an AJAX request.
                jQuery.ajax({
                    type: "POST",
                    url: ajaxurl, // WordPress AJAX URL.
                    data: {
                        action: "save_custom_trigger", // Action hook to handle the AJAX request in PHP.
                        selected_trigger: selectedTrigger,
                        gftp_campaign_id: campaignId,
                    },
                    success: function(response) {
                        // Handle the response if needed.
                        console.log("Trigger saved successfully.");

                        // Update the triggers list after saving.
                        gftpDisplayCurrentTriggers('<?php echo esc_js(wp_create_nonce('custom_popup_nonce')); ?>');

                        // Reset the flag and enable the "Add Trigger" button.
                        triggerFormDisplayed = false;
                        addTriggerButton.disabled = false;

                        // Remove the form after saving.
                        triggerSettingsSection.removeChild(triggerForm);
                    },
                });
            }

            function gftpRemoveTriggerFromWordPress(action, campaignId, nonce) {
                // Prevent the default behavior of the button click.
                event.preventDefault();

                // Create an AJAX request.
                jQuery.ajax({
                    type: "POST",
                    url: ajaxurl, // WordPress AJAX URL.
                    data: {
                        action: "remove_custom_trigger", // Action hook to handle trigger removal in PHP.
                        removed_trigger_action: action,
                        removed_trigger_campaign_id: campaignId,
                        custom_popup_nonce_field: nonce, // Add the nonce field.
                    },
                    success: function(response) {
                        // Handle the response if needed.
                        console.log("Trigger removed successfully.");
                    },
                    error: function(xhr, status, error) {
                        console.error("Error removing trigger:", error);
                    },
                });
            }

            function gftpCheckTriggerLimit() {
                // Check the number of triggers.
                if (triggers.length >= 10) {
                    addTriggerButton.disabled = true; // Disable the button if the limit is reached.
                } else {
                    addTriggerButton.disabled = false; // Enable the button if there's room for more triggers.
                }
            }

            // AJAX function to show current triggers.
            function gftpDisplayCurrentTriggers(nonce) {
                // Clear existing trigger rows.
                const triggersList = document.querySelector(".triggers-list");
                triggersList.innerHTML = "";

                // AJAX request to retrieve and display current triggers.
                jQuery.ajax({
                    type: 'POST',
                    url: ajaxurl, // WordPress AJAX URL.
                    data: {
                        action: 'get_current_triggers', // New action hook for retrieving triggers.
                        custom_popup_nonce_field: nonce, // Add nonce field.
                    },
                    success: function(response) {
                        // Handle the response if needed.
                        console.log("Triggers retrieved successfully.");

                        // Process and display the retrieved triggers here.
                        var triggerData = response;

                        // Check if trigger data is available.
                        if (triggerData.length > 0) {
                            // Iterate through the trigger data array.
                            for (var i = 0; i < triggerData.length; i++) {
                                var trigger = triggerData[i];

                                // Access trigger properties, e.g., trigger.action and trigger.gftp_campaign_id.
                                gftpDisplaySavedTrigger(trigger.action, trigger.gftp_campaign_id);
                            }
                        } else {
                            // Handle the case where no triggers are available.
                            console.log('No triggers have been added yet.');
                        }
                    },
                    error: function(xhr, status, error) {
                        // Handle the error here.
                        console.error('Error fetching triggers:', error);
                    },
                });
            }


            function gftpCreateTriggerForm() {
                triggerForm = document.createElement("div");
                triggerForm.classList.add("trigger-form");
                triggerForm.innerHTML = `
        <h3>Add Trigger</h3>
        <select class="trigger-dropdown">
          <option value="woocommerce_order_status_completed">Purchase</option>
          <option value="woocommerce_add_to_cart">Add to Cart</option>
          <option value="wp_login">Login</option>
          <option value="save_post">Save Post</option>
          <!-- Add other trigger options here -->
        </select>
        <input type="text" class="campaign-id-input" placeholder="Enter Campaign ID">
        <button class="button button-primary save-trigger-btn" name="custom_popup_update">Save</button>
      `;

                const saveTriggerButton = triggerForm.querySelector(".save-trigger-btn");
                saveTriggerButton.addEventListener("click", function() {
                    event.preventDefault(); // Prevent the default form submission.

                    const selectedTrigger = triggerForm.querySelector(".trigger-dropdown").value;
                    const campaignId = triggerForm.querySelector(".campaign-id-input").value;

                    // Save the trigger to WordPress using AJAX.
                    gftpSaveTriggerToWordPress(selectedTrigger, campaignId);

                    // Store the trigger in the triggers array.
                    triggers.push({
                        action: selectedTrigger,
                        gftp_campaign_id: campaignId
                    });

                    console.log('triggers: ', triggers);

                    // Check if triggerForm is still a child of triggerSettingsSection.
                    if (triggerForm.parentElement === triggerSettingsSection) {
                        // Remove the form after saving.
                        triggerSettingsSection.removeChild(triggerForm);
                        triggerFormDisplayed = false;
                    }
                });

                return triggerForm;
            }

            // Function to display saved trigger.
            function gftpDisplaySavedTrigger(trigger, campaignId) {
                if (trigger && campaignId) {
                    const triggersList = document.querySelector(".triggers-list");
                    const triggerRow = document.createElement("div");
                    triggerRow.classList.add("trigger-row");
                    triggerRow.innerHTML = `
        <div class="trigger-info">
          <span class="trigger-name">${trigger}</span>
          <span class="campaign-id">${campaignId}</span>
        </div>
        <button class="remove-trigger-btn" data-action="${trigger}" data-campaign-id="${campaignId}">
          <span class="dashicons dashicons-trash"></span> <!-- Use an icon (font-awesome in this case) for Remove -->
        </button>
        `;

                    const removeTriggerButton = triggerRow.querySelector(".remove-trigger-btn");
                    removeTriggerButton.addEventListener("click", function() {
                        const action = this.getAttribute("data-action");
                        const campaignId = this.getAttribute("data-campaign-id");

                        // Make an AJAX request to remove the trigger.
                        gftpRemoveTriggerFromWordPress(action, campaignId, '<?php echo esc_js(wp_create_nonce('custom_popup_nonce')); ?>');

                        // Remove the trigger row from the UI
                        triggersList.removeChild(triggerRow);
                    });

                    triggersList.appendChild(triggerRow);
                }
            }
        });
    </script>
<?php
}
