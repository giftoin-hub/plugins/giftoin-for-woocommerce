<div class="section payment-settings">
  <h2>Payment</h2>
  <p class="description">The Plugin support commonly payment methods, Below are some of them.</p>

  <?php
  // Define payment plugins with icons/images
  $payment_plugins = array(
    'PayPal' => 'paypal.svg',
    'Stripe' => 'stripe.svg',
    'Authorize.Net' => 'creditcard.svg',
    'Amazon Pay for WooCommerce' => 'amazon-pay.svg',
  );

  // Loop through payment plugins
  foreach ($payment_plugins as $plugin_name => $icon_filename) :
    ?>
      <div class="payment-plugin-container">
        <div class="payment-plugin">
          <div class="payment-icon">
            <img src="<?php echo esc_url(plugin_dir_url(__FILE__) . '../gftp-icons/' . $icon_filename); ?>" alt="<?php echo esc_attr($plugin_name); ?> Icon">
          </div>
          <div class="payment-name"><?php echo esc_html($plugin_name); ?></div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>