<div class="section cashback-settings">
  <h2>Settings</h2>
  <p class="description">Set Up Your Giftoin Integration Configuration Preferences.</p>
  <div class="wrap custom-popup-page">
    <form id="custom-popup-form" method="post" action="">
      <!-- Section for general Settings -->
      <div class="section general-settings">
        <h2>General</h2>
        <div class="form-row">
          <div class="form-label">
            <label for="gftp_api_key">Enter API Key</label>
          </div>
          <div class="form-input">
            <?php if (current_user_can('manage_options')) : ?>
              <input type="password" id="gftp_api_key" name="gftp_api_key" value="<?php echo esc_attr($gftp_api_key); ?>" <?php echo current_user_can('manage_options') ? '' : 'disabled'; ?>>
              <label for="reveal_api_key"><input type="checkbox" id="reveal_api_key"> Show API Key</label>
            <?php else : ?>
              <input type="password" id="gftp_api_key" name="gftp_api_key" value="" disabled>
            <?php endif; ?>
          </div>
          <p class="description">Copy & paste the Campaign Id from the Management Dashboard.</p>
          <p class="description">Navigate to <a href="https://reseller.giftoin.org/">https://reseller.giftoin.org/</a>.</p>
        </div>
      </div>
      <!-- Section for Cashback Settings (crypto currency) -->
      <div class="section cashback-settings">
        <h2>Cashback - Crypto Currency</h2>
        <p class="description">All fields are mandatory.</p>
        <div class="form-row">
          <div class="form-label">
            <label for="gftp_cashback_percentage">Campaign Id:</label>
          </div>
          <div class="form-input">
            <input type="text" id="gftp_cashback_campaign_id" name="gftp_cashback_campaign_id" value="<?php echo esc_attr($gftp_cashback_campaign_id); ?>">
          </div>
          <p class="description">Copy & paste the Campaign Id from the Management Dashboard.</p>
        </div>
        <div class="form-row">
          <div class="form-label">
            <label for="gftp_products_tag">Custom Tag:</label>
          </div>
          <div class="form-input">
            <input type="text" id="gftp_products_tag" name="gftp_products_tag" value="<?php echo esc_attr($gftp_products_tag); ?>">
          </div>
          <p class="description">Enter the custom tag (purchasing orders with this tag will trigger the crypto currency cashback)</p>
        </div>
        <div class="form-row">
          <div class="form-label">
            <label for="gftp_cashback_percentage">Coin Id:</label>
          </div>
          <div class="form-input">
            <input type="number" step="0.01" id="gftp_cashback_coin_id" name="gftp_cashback_coin_id" value="<?php echo esc_attr($gftp_cashback_coin_id); ?>">
          </div>
          <p class="description">Copy & paste it from the API request in the Management Dashboard.</p>
        </div>
        <div class="form-row">
          <div class="form-label">
            <label for="gftp_cashback_percentage">Percentage:</label>
          </div>
          <div class="form-input">
            <input type="number" step="0.01" id="gftp_cashback_percentage" name="gftp_cashback_percentage" value="<?php echo esc_attr($gftp_cashback_percentage); ?>">
          </div>
          <p class="description">Define the percentage refund for your customers (for each transaction they make on your website)</p>
        </div>
      </div>
      <!-- Section for Cashback Settings (user coin) -->
      <div class="section cashback-settings">
        <h2>Cashback - Your Token</h2>
        <p class="description">All fields are mandatory.</p>
        <div class="form-row">
          <div class="form-label">
            <label for="gftp_cashback_percentage">Campaign Id:</label>
          </div>
          <div class="form-input">
            <input type="text" id="gftp_ct_cashback_campaign_id" name="gftp_ct_cashback_campaign_id" value="<?php echo esc_attr($gftp_ct_cashback_campaign_id); ?>">
          </div>
          <p class="description">Copy & paste the Campaign Id from the Management Dashboard.</p>
        </div>
        <div class="form-row">
          <div class="form-label">
            <label for="gftp_ct_products_tag">Custom Tag:</label>
          </div>
          <div class="form-input">
            <input type="text" id="gftp_ct_products_tag" name="gftp_ct_products_tag" value="<?php echo esc_attr($gftp_ct_products_tag); ?>">
          </div>
          <p class="description">Enter the custom tag (purchasing orders with this tag will trigger the crypto currency cashback)</p>
        </div>
        <div class="form-row">
          <div class="form-label">
            <label for="gftp_cashback_percentage">Coin Id:</label>
          </div>
          <div class="form-input">
            <input type="number" step="0.01" id="gftp_ct_cashback_coin_id" name="gftp_ct_cashback_coin_id" value="<?php echo esc_attr($gftp_ct_cashback_coin_id); ?>">
          </div>
          <p class="description">Copy & paste it from the API request in the Management Dashboard.</p>
        </div>
        <div class="form-row">
          <div class="form-label">
            <label for="gftp_cashback_percentage">Percentage:</label>
          </div>
          <div class="form-input">
            <input type="number" step="0.01" id="gftp_ct_cashback_percentage" name="gftp_ct_cashback_percentage" value="<?php echo esc_attr($gftp_ct_cashback_percentage); ?>">
          </div>
          <p class="description">Define the percentage refund for your customers (for each transaction they make on your website)</p>
        </div>
      </div>
      <!-- Section for Rewarding Component Settings -->
      <div class="section popup-settings">
        <h2>Opt-in:</h2>
        <p>All fields are mandatory.</p>
        <p class="description">To display the rewarding component in your website, please copy and paste the shortcode below in the relevant pages</p>
        <pre><span class="highlight-shortcode">[gftp_rewarding_shortcode]</span></pre>
        <div class="form-row">
          <div class="form-label">
            <label for="gftp_rewarding_campaign_id">Campaign ID</label>
          </div>
          <div class="form-input">
            <input type="text" id="gftp_rewarding_campaign_id" name="gftp_rewarding_campaign_id" value="<?php echo esc_attr($gftp_custom_rewarding_campaign_id); ?>">
          </div>
          <p class="description">Copy & paste the Campaign Id from the Management Dashboard.</p>
        </div>
      </div>
      <!-- Section for triggers Settings -->
      <div class="section trigger-settings">
        <h2>Custom Trigger Settings</h2>
        <p>Here you can define custom triggers for advanced campaigns.</p>
        <button class="button button-secondary p-b" id="add_trigger_btn">Add Trigger</button>
        <div class="triggers-list">
          <!-- Saved triggers will be displayed here -->
        </div>
      </div>
      <button type="submit" class="button button-primary" name="custom_popup_update">Save Settings</button>
      <?php wp_nonce_field('custom_popup_nonce', 'custom_popup_nonce_field'); ?>
    </form>
  </div>
</div>

