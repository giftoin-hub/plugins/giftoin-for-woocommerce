<div class="wrap custom-popup-page">
  <!-- Your existing code for the logo, title, and description -->

  <!-- Tab Navigation -->
  <h2 class="nav-tab-wrapper">
    <a href="#getting-started" class="nav-tab nav-tab-active">Getting Started</a>
    <a href="#settings" class="nav-tab">Settings</a>
    <a href="#payment" class="nav-tab">Payment</a> <!-- New Payment Tab -->
  </h2>


  <!-- Tab Content -->
  <div class="tab-content" id="getting-started">
    <?php include(plugin_dir_path(__FILE__) . 'tab-getting-started.php'); ?>
  </div>

  <div class="tab-content" id="settings" style="display: none;">
    <?php include(plugin_dir_path(__FILE__) . 'tab-settings.php'); ?>
  </div>

  <div class="tab-content" id="payment" style="display: none;">
    <?php include(plugin_dir_path(__FILE__) . 'tab-payment.php'); ?>
  </div>

  <script>
    // JavaScript to handle tab switching
    document.addEventListener("DOMContentLoaded", function() {
      const tabs = document.querySelectorAll(".nav-tab");
      const tabContents = document.querySelectorAll(".tab-content");

      tabs.forEach((tab) => {
        tab.addEventListener("click", (e) => {
          e.preventDefault();
          tabs.forEach((t) => t.classList.remove("nav-tab-active"));
          tab.classList.add("nav-tab-active");
          const tabId = tab.getAttribute("href").replace("#", "");
          tabContents.forEach((content) => {
            if (content.id === tabId) {
              content.style.display = "block";
            } else {
              content.style.display = "none";
            }
          });
        });
      });
    });
  </script>
</div>