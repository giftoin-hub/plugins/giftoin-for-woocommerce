<?php

/**
 * Third party api keys to interact with.
 *
 * @author Giftoin Development Team <contact@giftoin.com>
 *
 * @license GPL2 https://www.gnu.org/licenses/gpl-2.0.html
 */
define('GIFTOIN_BASIC_API_URL', 'https://api.giftoin.org/api/v1/gcm/reseller/campaign/');
