<?php

/**
 * Cashback campaign trigger for order completion 
 * for products that have the custom cashback tag
 * 
 * send Giftoin to recipient with x% cashback as crypto currency
 */
function gftp_trigger_cashback_api_request_on_order_completion($order_id)
{
  if (
    in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))) &&
    get_option('gftp_cashback_percentage') !== false && // Check if cashback percentage is set
    get_option('gftp_cashback_campaign_id') !== false && // Check if campaign ID is set
    get_option('gftp_cashback_coin_id') !== false && // Check if coin ID is set
    get_option('gftp_products_tag') !== false && // Check if custom tag is set
    get_option('gftp_api_key') !== false // Check if the api key is set
  ) {

    // Include WooCommerce functions
    include_once WP_PLUGIN_DIR . '/woocommerce/woocommerce.php';

    // Get the order object
    $order = wc_get_order($order_id);
    $user_email = $order->get_billing_email();

    $custom_tag = get_option('gftp_products_tag');
    $total_price = 0;
    $has_custom_tag = false;

    // Check if order product contain the custom tag
    // Get the order items
    $order_items = $order->get_items();

     // Get the order currency
     $order_currency = $order->get_currency();

    foreach ($order_items as $item_id => $item) {
      $product_id = $item->get_product_id();
      $product = wc_get_product($product_id);
      $product_currency = get_woocommerce_currency();
      $product_price = $product->get_price();
      $product_tags = $product->get_tag_ids();

      foreach ($product_tags as $tag_id) {
        $tag = get_term($tag_id, 'product_tag');
        if ($tag && !is_wp_error($tag) && $custom_tag == $tag->name) {
          $total_price += $product_price;
          $has_custom_tag = true;
          break;
        }
      }
    };


    if (!$has_custom_tag) {
      return;
    };

    $gftp_api_key = get_option('gftp_api_key');
    $gftp_campaign_id = get_option('gftp_cashback_campaign_id');
    $gftp_cashback_coin_id = get_option('gftp_cashback_coin_id');
    $gftp_cashback_percentage = get_option('gftp_cashback_percentage');

    if ($total_price <= 0) {
      return;
    };

    $cashback_amount = number_format(($gftp_cashback_percentage / 100) * $total_price, 2);

    if ($cashback_amount <= 0.01) {
      return;
    };

    $giftoin_api = GIFTOIN_BASIC_API_URL;

    // Prepare the API request data
    $api_url = $giftoin_api . $gftp_campaign_id . '/order';
    $api_data = array(
      'contacts' => array($user_email),
      'coins' => array(
        array(
          'id' => (int)$gftp_cashback_coin_id,
          'valueInUSD' => (float)$cashback_amount, // Assuming $cashback_amount represents the value in USD
          'currency' => $order_currency
        )
      )
    );

    // Set the request parameters
    $request_args = array(
      'body' => wp_json_encode($api_data),
      'headers' => array(
        'Content-Type' => 'application/json',
        'x-gft_api_key' => $gftp_api_key, // Include the API key in the header
      ),
    );

    // Perform the API POST request using wp_safe_remote_post
    $response = wp_safe_remote_post($api_url, $request_args);

    if (is_wp_error($response)) {
      // Handle WP error if needed
      error_log('WP error: ' . esc_html($response->get_error_message())); // Escaped output
    } else {
      $response_code = wp_remote_retrieve_response_code($response);

      if ($response_code === 200) {
        error_log('API response: ' . esc_html(wp_remote_retrieve_body($response))); // Escaped output
      } else {
        error_log('Non-200 response: ' . esc_html($response_code)); // Escaped output
      };
    };
  };
};
add_action('woocommerce_order_status_completed', 'gftp_trigger_cashback_api_request_on_order_completion');

function gftp_trigger_ct_cashback_api_request_on_order_completion($order_id)
{
  if (
    in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))) &&
    get_option('gftp_ct_cashback_percentage') !== false && // Check if cashback percentage is set
    get_option('gftp_ct_cashback_campaign_id') !== false && // Check if campaign ID is set
    get_option('gftp_ct_cashback_coin_id') !== false && // Check if coin ID is set
    get_option('gftp_ct_products_tag') !== false && // Check if custom tag is set
    get_option('gftp_api_key') !== false // Check if the api key is set
  ) {
    // Include WooCommerce functions
    include_once WP_PLUGIN_DIR . '/woocommerce/woocommerce.php';

    // Get the order user email
    $order = wc_get_order($order_id);
    $user_email = $order->get_billing_email();

    $custom_tag = get_option('gftp_ct_products_tag');
    $total_price = 0;
    $has_custom_tag = false;

    // Check if order product contain the custom tag
    // Get the order items
    $order_items = $order->get_items();

    // Get the order currency
    $order_currency = $order->get_currency();

    foreach ($order_items as $item_id => $item) {
      $product_id = $item->get_product_id();
      $product = wc_get_product($product_id);
      $product_currency = get_woocommerce_currency();
      $product_price = $product->get_price();
      $product_tags = $product->get_tag_ids();

      $has_tag = false;
      foreach ($product_tags as $tag_id) {
        $tag = get_term($tag_id, 'product_tag');
        if ($tag && !is_wp_error($tag) && $custom_tag == $tag->name) {
          $total_price += $product_price;
          $has_custom_tag = true;
          $has_tag = true;
          break;
        }
      }
    };

    if (!$has_custom_tag) {
      return;
    };

    $gftp_api_key = get_option('gftp_api_key');
    $gftp_campaign_id = get_option('gftp_ct_cashback_campaign_id');
    $gftp_cashback_coin_id = get_option('gftp_ct_cashback_coin_id');
    $gftp_cashback_percentage = get_option('gftp_ct_cashback_percentage');

    if ($total_price <= 0) {
      return;
    };

    $cashback_amount = number_format(($gftp_cashback_percentage / 100) * $total_price, 2);

    if ($cashback_amount <= 0.01) {
      return;
    };

    $giftoin_api = GIFTOIN_BASIC_API_URL;

    // Prepare the API request data
    $api_url = $giftoin_api . $gftp_campaign_id . '/order';
    $api_data = array(
      'contacts' => array($user_email),
      'coins' => array(
        array(
          'id' => (int)$gftp_cashback_coin_id,
          'valueInUSD' => (float)$cashback_amount, // Assuming $cashback_amount represents the value in USD
          'currency' => $order_currency
        )
      )
    );

    // Set the request parameters
    $request_args = array(
      'body' => wp_json_encode($api_data),
      'headers' => array(
        'Content-Type' => 'application/json',
        'x-gft_api_key' => $gftp_api_key, // Include the API key in the header
      ),
    );

    // Perform the API POST request using wp_safe_remote_post
    $response = wp_safe_remote_post($api_url, $request_args);

    if (is_wp_error($response)) {
      // Handle WP error if needed
      error_log('WP error: ' . esc_html($response->get_error_message())); // Escaped output
    } else {
      $response_code = wp_remote_retrieve_response_code($response);

      if ($response_code === 200) {
        // API request was successful, log the response for debugging
        error_log('API response: ' . esc_html(wp_remote_retrieve_body($response))); // Escaped output
      } else {
        // Handle non-200 response if needed
        error_log('Non-200 response: ' . esc_html($response_code)); // Escaped output
      }
    }
  }
}
add_action('woocommerce_order_status_completed', 'gftp_trigger_ct_cashback_api_request_on_order_completion');


function gftp_save_custom_trigger()
{
  // Retrieve the trigger data sent via AJAX
  $selected_trigger = sanitize_text_field($_POST["selected_trigger"]);
  $gftp_campaign_id = sanitize_text_field($_POST["gftp_campaign_id"]);

  // Save the trigger data to WordPress, e.g., as an option
  $triggers = get_option("custom_popup_triggers", []);

  // Limit the number of triggers to a maximum of 10
  if (count($triggers) < 10) {
    // Save the trigger data to WordPress, e.g., as an option
    $triggers[] = [
      "action" => $selected_trigger,
      "gftp_campaign_id" => $gftp_campaign_id,
    ];
    update_option("custom_popup_triggers", $triggers);

    // Return a success response if needed
    echo "Trigger saved successfully.";
  } else {
    // Return an error response if the maximum limit is reached
    echo "Maximum limit of triggers reached. You cannot save more than 10 triggers.";
  }

  wp_die(); // Always include this to terminate the AJAX request properly
}
add_action("wp_ajax_save_custom_trigger", "gftp_save_custom_trigger");
add_action("wp_ajax_nopriv_save_custom_trigger", "gftp_save_custom_trigger");

function gftp_get_current_triggers()
{
  // Verify nonce
  $nonce = sanitize_text_field($_POST['custom_popup_nonce']);
  if (!isset($nonce) || !wp_verify_nonce($nonce, 'custom_popup_nonce')) {
    wp_send_json_error('Nonce verification failed.'); // Use wp_send_json_error for AJAX requests
  }

  // Retrieve the current triggers
  $triggers = get_option('custom_popup_triggers', []);

  // Initialize an array to store trigger data
  $trigger_data = [];

  // Check if there are any triggers
  if (!empty($triggers)) {
    foreach ($triggers as $trigger) {
      $trigger_data[] = [
        'action' => esc_html($trigger['action']),
        'gftp_campaign_id' => esc_html($trigger['gftp_campaign_id']),
      ];
    }
  } else {
    $trigger_data[] = [
      'message' => 'No triggers have been added yet.',
    ];
  }

  // Send the trigger data as a JSON response
  wp_send_json_success($trigger_data);

  // Always exit after handling AJAX requests
  wp_die();
}
add_action('wp_ajax_get_current_triggers', 'gftp_get_current_triggers');
add_action('wp_ajax_nopriv_get_current_triggers', 'gftp_get_current_triggers');


function gftp_remove_custom_trigger()
{
  // Verify nonce
  $nonce_field = 'custom_popup_nonce_field';
  $nonce_value = isset($_POST[$nonce_field]) ? sanitize_text_field($_POST[$nonce_field]) : '';

  if (!wp_verify_nonce($nonce_value, 'custom_popup_nonce')) {
    wp_send_json_error('Nonce verification failed.', 403);
    wp_die();
  };

  // Get the removed trigger's action and campaign ID
  $removed_trigger_action = sanitize_text_field($_POST['removed_trigger_action']);
  $removed_trigger_campaign_id = sanitize_text_field($_POST['removed_trigger_campaign_id']);

  // Retrieve the current triggers
  $triggers = get_option('custom_popup_triggers', []);

  // Find the index of the trigger to be removed
  $index = -1;
  foreach ($triggers as $key => $trigger) {
    if ($trigger['action'] === $removed_trigger_action && $trigger['gftp_campaign_id'] === $removed_trigger_campaign_id) {
      $index = $key;
      break;
    }
  }

  // If the trigger was found, remove it
  if ($index !== -1) {
    unset($triggers[$index]);
    $triggers = array_values($triggers);
    update_option('custom_popup_triggers', $triggers);

    wp_send_json_success(array('message' =>  'Trigger successfully removed'));
  } else {
    // Send an error response
    wp_send_json_error(array('message' => 'Trigger not found'));
  }

  // Always exit after handling AJAX requests
  wp_die();
}
add_action('wp_ajax_remove_custom_trigger', 'gftp_remove_custom_trigger');
add_action('wp_ajax_nopriv_remove_custom_trigger', 'gftp_remove_custom_trigger');


function gftp_add_trigger_actions()
{
  // Retrieve the current triggers
  $triggers = get_option('custom_popup_triggers', []);

  foreach ($triggers as $trigger) {
    $action = $trigger['action'];
    $gftp_campaign_id = $trigger['gftp_campaign_id'];

    // Define the action hook based on the trigger's action
    $action_hook = $action;

    // Define the callback function based on the trigger's campaign ID
    $callback_function = function () use ($gftp_campaign_id) {
      // Your callback logic here, e.g., trigger API request with $gftp_campaign_id
      gftp_trigger_api_request($gftp_campaign_id);
    };

    // Add the action hook and callback function
    add_action($action_hook, $callback_function, 20, 3);
  }
}
add_action('init', 'gftp_add_trigger_actions');

function gftp_trigger_api_request($gftp_campaign_id)
{
  // Get the current logged-in user's email
  $current_user = wp_get_current_user();
  $current_user_email = $current_user->user_email;
  $gftp_api_key = get_option('gftp_api_key');

  $giftoin_api = GIFTOIN_BASIC_API_URL;

  // Prepare the API request data
  $api_url = $giftoin_api . $gftp_campaign_id . '/order';
  $api_data = array(
    'contacts' => array($current_user_email),
  );

  // Set the request parameters
  $request_args = array(
    'body' => json_encode($api_data),
    'headers' => array(
      'Content-Type' => 'application/json',
      'x-gft_api_key' => $gftp_api_key, // Include the API key in the header
    ),
  );

  // Perform the API POST request using wp_safe_remote_post
  $response = wp_safe_remote_post($api_url, $request_args);

  if (is_wp_error($response)) {
    // Handle WP error if needed
    error_log('WP error: ' . esc_html($response->get_error_message()));
    wp_die();
  } else {
    $response_code = wp_remote_retrieve_response_code($response);

    if ($response_code === 200) {
      // API request was successful, log the response for debugging
      error_log('API response: ' . esc_html(wp_remote_retrieve_body($response)));
    } else {
      // Handle non-200 response if needed
      error_log('Non-200 response: ' . esc_html(wp_remote_retrieve_body($response)));
    }
  }

  wp_die();
}

function gftp_rewarding_custom_api()
{
  // Verify nonce
  $nonce_field = 'custom_popup_nonce_field';
  $nonce_value = isset($_POST[$nonce_field]) ? sanitize_text_field($_POST[$nonce_field]) : '';

  if (!wp_verify_nonce($nonce_value, 'custom_popup_nonce')) {
    wp_send_json_error('Nonce verification failed.', 403);
    wp_die();
  };

  $user_email = sanitize_email($_POST['user_email']); // Retrieve the email from the POST data
  $gftp_api_key = get_option('gftp_api_key');
  $gftp_campaign_id = get_option('gftp_custom_rewarding_campaign_id');

  $giftoin_api = GIFTOIN_BASIC_API_URL;

  // Prepare the API request data
  $api_url = $giftoin_api . $gftp_campaign_id . '/order';
  $api_data = array(
    'contacts' => array($user_email),
  );

  // Set the request parameters
  $request_args = array(
    'body' => json_encode($api_data),
    'headers' => array(
      'Content-Type' => 'application/json',
      'x-gft_api_key' => $gftp_api_key, // Include the API key in the header
    ),
  );

  // Perform the API POST request using wp_safe_remote_post
  $response = wp_safe_remote_post($api_url, $request_args);

  if (is_wp_error($response)) {
    // Handle WP error if needed
    error_log('WP error: ' . esc_html($response->get_error_message()));
  } else {
    $response_code = wp_remote_retrieve_response_code($response);

    if ($response_code === 200) {
      // API request was successful, log the response for debugging
      error_log('API response: ' . esc_html(wp_remote_retrieve_body($response)));
    } else {
      // Handle non-200 response if needed
      error_log('Non-200 response: ' . esc_html($response_code));
    }
  }

  wp_die();
}
add_action('wp_ajax_rewarding_custom_api', 'gftp_rewarding_custom_api');
add_action('wp_ajax_nopriv_rewarding_custom_api', 'gftp_rewarding_custom_api');
