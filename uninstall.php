<?php

/**
 * If uninstall not called from WordPress, then exit.
 *
 * @author Giftoin Development Team <contact@giftoin.com>
 *
 * @license GPL2 https://www.gnu.org/licenses/gpl-2.0.html
 */
if (defined('WP_UNINSTALL_PLUGIN') === false) {
    exit;
}

// Delete plugin options from the database.
delete_option('gftp_api_key');
delete_option('gftp_campaign_id');
delete_option('gftp_products_tag');
delete_option('gftp_cashback_percentage');
delete_option('gftp_cashback_campaign_id');
delete_option('gftp_cashback_coin_id');
delete_option('gftp_ct_cashback_campaign_id');
delete_option('gftp_ct_products_tag');
delete_option('gftp_ct_cashback_percentage');
delete_option('gftp_ct_cashback_coin_id');
delete_option('gftp_rewarding_campaign_id');
