<?php
/*
Plugin Name:  Giftoin Plugin
Plugin URI:   https://giftoin.com/
Description:  Enhance your WooCommerce store with the Giftoin Plugin, allowing seamless integration with the Giftoin platform. Reward your customers with custom triggers, offer cashback campaigns, and engage with Giftoin's unique features, all from within your WooCommerce store. Boost customer loyalty and sales with Giftoin integration today.
Version:      1.0.0
Author:       Giftoin
Author URI:   https://giftoin.com/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  wpb-tutorial
Domain Path:  /languages
*/

if (!defined('ABSPATH')) exit;

// Include necessary files
require_once plugin_dir_path(__FILE__) . 'gftp-admin/admin-page-functions.php';
require_once plugin_dir_path(__FILE__) . 'gftp-popup/popup-shortcode.php';
require_once plugin_dir_path(__FILE__) . 'gftp-rewarding/rewarding-shortcode.php';
require_once plugin_dir_path(__FILE__) . 'gftp-ajax/ajax-functions.php';
require_once plugin_dir_path(__FILE__) . 'config.php';

// Enqueue styles and scripts
function giftoin_enqueue_styles_and_scripts()
{
  wp_enqueue_style('styles', plugin_dir_url(__FILE__) . 'gftp-resources/css/styles.css');
  wp_enqueue_script('popup-script', plugin_dir_url(__FILE__) . 'gftp-popup/popup-script.js', array('jquery'), null, true);
  wp_enqueue_script('rewarding-script', plugin_dir_url(__FILE__) . 'gftp-rewarding/rewarding-script.js', array('jquery'), null, true);
  wp_enqueue_style('giftoin-icon', plugin_dir_url(__FILE__) . 'gftp-rewarding/gift.svg');

  // Pass the AJAX URL to the script
  wp_localize_script('popup-script', 'popup_script_vars', array(
    'ajax_url' => admin_url('admin-ajax.php'),
    'nonce' => wp_create_nonce('custom_popup_nonce'), // Generate nonce here
  ));

}
add_action('wp_enqueue_scripts', 'giftoin_enqueue_styles_and_scripts');

function giftoin_admin_enqueue_styles_and_scripts() 
{
  wp_enqueue_style('styles-admin', plugin_dir_url(__FILE__) . 'gftp-admin/styles.css');
}
add_action('admin_enqueue_scripts', 'giftoin_admin_enqueue_styles_and_scripts');

// Add admin menu page
function giftoin_plugin_menu()
{
  add_menu_page(
    'Giftoin',
    'Giftoin',
    'manage_options',
    'giftoin-plugin',
    'gftp_plugin_page',
    'data:image/svg+xml;base64,' . base64_encode(file_get_contents(plugin_dir_path(__FILE__) . 'gftp-icons/gift.svg')), // Replace 'your-custom-icon.svg' with your actual icon file name
    'dashicons-gift',
    99
  );
}
add_action('admin_menu', 'giftoin_plugin_menu');

function gftp_add_ajax_url_to_frontend()
{
  // Enqueue the 'ajaxurl' JavaScript variable
  wp_enqueue_script('ajax-js', admin_url('admin-ajax.php'));
}
add_action('wp_enqueue_scripts', 'gftp_add_ajax_url_to_frontend');

function gftp_enqueue_dashicons()
{
  wp_enqueue_style('dashicons');
}

add_action('admin_enqueue_scripts', 'gftp_enqueue_dashicons');
