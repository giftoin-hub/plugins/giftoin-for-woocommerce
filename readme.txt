=== Giftoin - Web3 Based Loyalty & Cashback ===
Contributors: Giftoin
Tags: api, cashback, loyalty, web3, blockchain
Requires at least: 4.0
Tested up to: 6.4.2
Stable tag: 1.0.0
Requires PHP: 8.2
License: GPL-3.0
License URI: https://www.gnu.org/licenses/gpl-3.0.html

== Description ==
**Enhance customer engagement with Web3 loyalty programs**, unlock unique cashback opportunities, and harness the full potential of your online store with the Giftoin WordPress plugin for WooCommerce. Experience the transformative impact of Web3 on your e-commerce platform.

To fully utilize this plugin, registration as a reseller on the Giftoin platform is required. Boost your WooCommerce store’s performance and customer satisfaction with the Giftoin WordPress plugin, a cornerstone of the web3 ecosystem"

== Installation ==
1. Log in to WordPress admin.
2. Go to Plugins > Add New.
3. Search for the Giftoin for the WooCommerce plugin.
4. Click on Install Now and wait until the plugin is installed successfully.
5. Activate the plugin through the 'Plugins' menu in WordPress.
6. Configure the plugin settings under 'Settings' in the WordPress admin panel.
7. Use the provided shortcodes or hooks to trigger Giftoin API requests based on your requirements.

== Configuration ==
-  Documentation can be found on [Our Docs](https://docs.giftoin.org/ "Giftoin Docs") 
1. Navigate to 'Settings' -> 'Giftoin' in your WordPress admin panel.
2. Enter your Giftoin API Key and Campaign ID.
3. The "Percentage" field is the percentage amount to return as cashback to the user (cashback will be calculated as USD).
4. Add a "Custom Tag" to your Giftoin cashback.
5. Add the "Custom tag" to one or more of your products to trigger the Giftoin Cashback.
6. Configure other plugin settings as needed.

== Usage ==
- To trigger Giftoin API requests based on specific actions, use the provided hooks or shortcodes.
- Manage your triggers via the WordPress admin panel under 'Giftoin Triggers.'
- For detailed usage instructions and examples, refer to the documentation on the plugin's official website.

== Frequently Asked Questions ==
= How do I obtain a Giftoin API Key and Campaign ID? =
You can obtain these credentials by signing up for a Giftoin reseller account. 
Visit the dashboard for more information [Giftoin Dashboard](https://reseller.giftoin.org/ "Giftoin Dashboard") .

== Third-Party Services ==
= Giftoin =
The giftoin API is used to send cashback/rewards/tokens in the form of cryptocurrency / NFT tokens to the end user upon ordering dedicated items on your store.
Giftoin TOS - https://giftoin.com/terms_of_service/
Giftoin Privacy Policy & AML - https://giftoin.com/privacy_policy_and_aml/
= Firebase =
This plugin uses a third-party service to hold our public files. The service is provided by https://firebase.google.com/
Firebase TOS - https://firebase.google.com/terms.

= Where can I find more information or get support for this plugin? =
For documentation visit [Our Docs](https://docs.giftoin.org/ "Giftoin Docs").
For support you have multiple options:
1. [Telegram Channel](https://t.me/+wuTIKXTglyE1MmE0 "Telegram Channel")
2. Email - support@giftoin.com
3. Create a support ticket [here](https://wordpress.org/support/plugin/giftoin-for-woocommerce/ "WordPress Support Section") in WordPress 

== Changelog ==

= 1.0.0 - 2023-09-27 =
* Initial release.

== Upgrade Notice ==
There are no special upgrade instructions for this version.
